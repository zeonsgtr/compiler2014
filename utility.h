#ifndef FILE_UTILITY_H
#define FILE_UTILITY_H

void report_error(char*);

#define WIDTH_CHAR (1)
#define WIDTH_VOID (0)
#define MAX_PARS_PRINTF (6)

//  #define ASSERT_ON
//  #define COLOR_ON
//  #define PRINT_AST
//  #define PRINT_SYMBOL_TABLE
//  #define IR_TO_MIPS_ONE_BY_ONE

//  #define DEBUG
//  #define PRINT_IR

#endif
