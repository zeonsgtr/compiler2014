#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "utility.h"

void report_error(char* s) {
    puts(s);
    exit(1);
}
