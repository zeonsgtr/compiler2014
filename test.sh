#!/bin/bash

timelimit=20s
dir=$(pwd)/compiler2014-testcases/Normal
make all
pwd
for name in $(./sort_by_size.sh); do
    cd bin/
    echo -e "\033[31;1m[RUNNING]\033[0m \033[32m$name\033[0m"
    cp ../compiler2014-testcases/Normal/$name.c $name.c
    gcc $name.c -o $name &>/dev/null
    ./$name >1.out
    (timeout $timelimit ./c_compiler $name.c) >ass_code.s 2>/dev/null
    (timeout $timelimit spim -stat -file ass_code.s) >2.out 2>hehe #2>/dev/null
    cat hehe
    diff -w 1.out 2.out >$name.log
    if [ -s $name.log ]; then
        echo -e "\033[34;1mNo\033[0m"
        #exit
    else
        echo -e "\033[34;1mYes\033[0m"
    fi
    rm $name.log
    rm $name $name.c 1.out 2.out ass_code.s
    cd ..
done
