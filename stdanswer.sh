#!/bin/bash

dir=$(pwd)/testcases/Normal
out=gccresults
mkdir -p $out
for tcase in $(ls $dir/*.c); do
    name=$(echo $tcase | grep "[-a-zA-Z0-9_]*\.c" -o | cut -f1 -d".")
    echo -e "\033[31;1m[RUNNING]\033[0m \033[32m$name\033[0m"
    cd $out/
    cp $tcase .
    gcc $name.c -o $name
    ./$name >$name.gcc_out
    rm $name
    rm $name.c
    cd ..
done
