all: main.c semantics.c semantics.h ast.c analyzer.tab.c lex.yy.c analyzer.tab.h \
	def.h ast.h utility.c utility.h translate.c translate.h interprete.c interprete.h optimize.c optimize.h
	gcc -o c_compiler main.c utility.c analyzer.tab.c ast.c lex.yy.c semantics.c translate.c interprete.c optimize.c -O2
	mkdir -p bin/
	cp builtin.s bin/builtin.s
	cp c_compiler bin/c_compiler

warning: main.c semantics.c semantics.h ast.c analyzer.tab.c lex.yy.c analyzer.tab.h \
    def.h ast.h utility.c utility.h translate.c translate.h
	gcc -o c_compiler main.c utility.c analyzer.tab.c ast.c lex.yy.c semantics.c translate.c interprete.c -Wall

parser: parser.c ast.c analyzer.tab.c lex.yy.c analyzer.tab.h def.h ast.c utility.c utility.h
	gcc -o parser parser.c utility.c analyzer.tab.c ast.c lex.yy.c

analyzer.tab.h: analyzer.y
	bison -d analyzer.y

analyzer.tab.c: analyzer.y
	bison -d analyzer.y

lex.yy.c: analyzer.l
	flex analyzer.l

clean:
	rm -rf analyzer.tab.c analyzer.tab.h lex.yy.c bin/ data1 LC_PAPER=zh_CN.UTF-8 a.out 1.out 2.out 3.out \
	c_compiler parser data1.c data2.c x.s *.aux *.log *.toc
